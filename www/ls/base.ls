soucty = d3.tsv.parse ig.data.soucty, (row) ->
  for index, value of row
    continue if index == 'nazev'
    row[index] = parseInt row[index], 10
  row['skutku-celkem'] = row['objasneno'] + row['objasneno-dodatecne']
  row['recidivistePrc'] = row['recidiviste'] / row['skutku-celkem']
  row['nerecidivistePrc'] = 1 - row['recidivistePrc']
  row['zbran-strelna'] = row['zbran-a'] + row['zbran-b'] + row['zbran-c'] + row['zbran-d'] + row['zbran-strelna-jina']
  for field in <[zbran-strelna zbran-a  zbran-b zbran-c zbran-d zbran-strelna-jina  vybusnina chladna zbran-jina  zbran-vyhruzka  zbran-jiny-predmet]>
    row["#{field}Prc"] = row[field] / row['se-zbrani']
  row

container = d3.select ig.containers.base
# soucty.length = 1
elements = container.selectAll \div.cin .data soucty .enter!append \div
  ..attr \class \cin
  ..append \h2 .html (.nazev)

bars = elements.append \div .attr \class \bars
scale = d3.scale.linear!
  ..domain [0 1]
  ..range [0 100]

draw = (fields, celkemField) ->
  for soucet in soucty
    soucet.bars = fields.map ({id, name, color}) ->
      value = soucet[id]
      width = "#{scale value}%"
      sum = soucet[celkemField]
      {id, name, value, width, soucet:parent, color, sum}
    soucet.bars .= filter -> it.value
  elements.classed \zbrane celkemField == 'se-zbrani'
  bars.selectAll \div.bar .data (.bars)
    ..enter!.append \div
      ..append \div .attr \class \label
        ..append \div .attr \class \border
        ..append \div .attr \class \content
    ..exit!remove!
    ..attr \class -> "bar #{it.id}"
    ..style \width (.width)
    ..select \.content
      ..html ->
        count = Math.round it.sum * it.value
        pripadu = switch
          | count == 1 => "případ"
          | 0 < count < 5 => "případy"
          | otherwise => "případů"
        "#{it.name}<br><b>#{ig.utils.formatNumber count}</b> #{pripadu}, <b>#{ig.utils.formatNumber it.value * 100} %</b>"

if window.location.hash == '#recidiva'
  recidivaFields =
    * id: \recidivistePrc
      name: "Recidivisté"
    * id: \nerecidivistePrc
      name: "Ostatní"

  draw recidivaFields, 'skutku-celkem'
else
  zbraneFields =
    * id: \zbran-strelnaPrc
      name: "Střelná zbraň"
    * id: \chladnaPrc
      name: "Chladná zbraň"
    * id: \vybusninaPrc
      name: "Výbušnina"
    * id: \zbran-jinaPrc
      name: "Jiná zbraň"
    * id: \zbran-jiny-predmetPrc
      name: "Jiný předmět"
    * id: \zbran-vyhruzkaPrc
      name: "Vyhrožování zbraní"

  draw zbraneFields, 'se-zbrani'
